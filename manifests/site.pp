## site.pp ##

## Active Configurations ##

# Disable filebucket by default for all File resources:
#https://docs.puppet.com/pe/2015.3/release_notes.html#filebucket-resource-no-longer-created-by-default
File { backup => false }

## DEFAULT NODE ##

# The default node definition matches any node
node default {
  hiera_include(classes)
}
