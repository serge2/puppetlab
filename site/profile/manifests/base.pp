class profile::base {

  # This profile contains company base for ALL nodes.
  # as such this should be included for all nodes.

  # a basic version check
  if (versioncmp($::puppetversion, '5.0.0') < 0 ) {
    fail("Only Puppet version 5 and higher is supported. You are running version: ${::puppetversion}")
  }

  # split on operatingsystem into a subclass that will contain the OS logic
  case $::kernel {
    'windows': {
      contain profile::windows::baseline
      }
    'Linux': {
      contain profile::linux::baseline
      }
    default: {
      fail("Operating system not supported. (found ${::operatingsystem} on ${$::kernel}  version ${::operatingsystemrelease})")
      }
  }
}
