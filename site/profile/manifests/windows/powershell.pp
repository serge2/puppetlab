#
class  profile::windows::powershell ()
{

  package { 'powershell':
    ensure          => latest,
    provider        => 'chocolatey',
    install_options => ['-pre'],
  }

}
