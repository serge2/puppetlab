#
class profile::windows::puppetagent(
  $runinterval = 1800
) {

  # make sure agent puppet.conf environment matches

  file{ "C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf":
    content => template( "profile/windows/agent_puppet.conf.erb" ),
  }
  
}
