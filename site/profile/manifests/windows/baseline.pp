#
# This profile is setup for all the 'profile::windows' subclasses
#
# it contains the windows logic
#
class profile::windows::baseline {

  include profile::windows::puppetagent
  include profile::windows::powershell
  include profile::windows::localadmins
  include kms_win
  include chocolatey
  include bginfo

}
